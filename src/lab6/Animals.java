package lab6;

public abstract class Animals {
	
	private String name;
	public Animals(String name){
		this.name = name;
		
	}

	public abstract String food();
	
	public String toString(){
		return this.name;
		
	}
	
}
